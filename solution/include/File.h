#ifndef FILE_H
#define FILE_H

#include "bmp.h"
#include "bmp_processing.h"
#include <inttypes.h>
#include <stdio.h>


enum read_status  {
    READ_OK = 0,
    READ_ERROR_SIGNATURE,
    READ_ERROR_BITS,
    READ_ERROR_DATA,
    READ_ERROR_HEADER,
    READ_ERROR_CONDITIONS
};
enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR_DATA,
    WRITE_ERROR_HEADER

};

enum close_file_status {
    CLOSE_OK = 0,
    CLOSE_ERROR
};

enum rights{
    READ_ONLY,
    WRITE_ONLY
};
enum result{
    YES,
    NO
};

struct file_check{
    FILE *file;
    enum result result;
};

struct file_check open_file( const char *path, enum rights rights1);
enum close_file_status close_file(FILE* file);
enum read_status read_file(FILE* file, struct image* image);
enum write_status write_file(FILE* file, struct image const* image);
#endif
