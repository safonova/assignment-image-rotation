#ifndef INTERNAL_FORMAT_H
#define INTERNAL_FORMAT_H

#include <inttypes.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

#endif
