#ifndef UTILS_H
#define UTILS_H
#include "internal_format.h"

void error_print(const char* status);
void free_image(struct image img);
#endif
