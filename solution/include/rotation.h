#include "bmp.h"
#include "internal_format.h"

#ifndef ROTATION_H
#define ROTATION_H
struct image rotation(struct image const* image);

#endif
