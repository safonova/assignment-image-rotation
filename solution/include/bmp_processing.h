#include "../include/bmp.h"
#include "../include/internal_format.h"
#include <stdbool.h>
#include <stdio.h>


#ifndef ENUMS_H
#define ENUMS_H

uint8_t padding(uint32_t width);
bool check_bmp_header(struct Bmp_header bmpHeader);

struct image create_image_by_bmp( struct Bmp_header* bmpHeader);

struct image create_new_image_by_image(struct image const* image);
struct Bmp_header made_bmp_header(struct image const* image);
#endif
