#include "../include/bmp.h"
#include "../include/File.h"
#include "../include/Utils.h"
#include "../include/bmp_processing.h"
#include "../include/rotation.h"

#include <stdio.h>

int main(int argc, char** argv ){
    if(argc < 3) return 1;

    const char* path_in = argv[1];
    const char* path_out = argv[2];


    struct image image = {0};

    struct file_check file_open_in = open_file(path_in,READ_ONLY);
    if(file_open_in.result == NO){
        fprintf(stderr,"\n%s","File is not open.");
        return 1;
    } else fprintf(stderr,"\n%s","File is open.");

    if(read_file(file_open_in.file, &image) != 0 || close_file(file_open_in.file)!= 0) {
        free_image(image);
        return 1;
    }

    struct image new_image = rotation(&image);

    struct file_check file_open_out = open_file(path_out,WRITE_ONLY);
    if(file_open_in.result == NO){
        fprintf(stderr,"\n%s","File is not open.");
        return 1;
    } else fprintf(stderr,"\n%s","File is open.");

    if ( write_file(file_open_out.file, &new_image) != 0 || close_file(file_open_out.file) != 0) {
        free_image(image);
        free_image(new_image);
        return 1;
    }

    free_image(image);
    free_image(new_image);

    return 0;
}



