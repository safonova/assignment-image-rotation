#include "../include/bmp.h"
#include "../include/internal_format.h"

#include <malloc.h>
#include <stdbool.h>
#include <stdlib.h>


struct image create_image_by_bmp( struct Bmp_header* bmpHeader){
    struct image out_image;
    out_image.width = bmpHeader->biWidth;
    out_image.height = bmpHeader->biHeight;
    out_image.data = malloc(bmpHeader->biSizeImage);
    return out_image;
}


uint8_t padding(uint32_t width) {
    for (uint8_t i = 0; i <4; i++ )
        if ((width * 3 + i) % 4 == 0) return i;
    return 0;
}

struct image create_new_image_by_image(struct image const* image){
    struct image new_image = {0};
    size_t padd = (uint8_t) padding(image->width);

    new_image.height = image->width;
    new_image.width = image->height;
    new_image.data = malloc(image->width * (image->height * 3 + padd));
    return new_image;
}


bool check_bmp_header(struct Bmp_header bmpHeader) {

    if(bmpHeader.bfReserved != 0
    || bmpHeader.bOffBits != 54
    || bmpHeader.biSize != 40
    || bmpHeader.biPlanes != 1
    || bmpHeader.biBitCount != 24
    || bmpHeader.biCompression !=0) return false;

    return true;
}


struct Bmp_header made_bmp_header(struct image const* image){
    struct Bmp_header bmpHeader;
    bmpHeader.bfType = 0x4d42;
    bmpHeader.bfileSize = sizeof(bmpHeader) + image->width * image->height * 3 + padding(image->width) * image->height * 3;
    bmpHeader.bfReserved = 0;
    bmpHeader.bOffBits = 54;
    bmpHeader.biSize = 40;
    bmpHeader.biWidth = image->width;
    bmpHeader.biHeight = image->height;
    bmpHeader.biPlanes = 1;
    bmpHeader.biBitCount = 24;
    bmpHeader.biCompression = 0;
    bmpHeader.biSizeImage = image->width * image->height * 3 + padding(image->width) * image->height;
    bmpHeader.biXPelsPerMeter = 2834;
    bmpHeader.biYPelsPerMeter =2834;
    bmpHeader.biClrUsed = 0;
    bmpHeader.biClrImportant = 0;
    return bmpHeader;
}
