#include "../include/bmp.h"
#include "../include/bmp_processing.h"

struct image rotation(struct image const* image){
    struct image new_image = create_new_image_by_image(image);

    for(uint64_t i = 0; i < image->width; i++)
        for(uint64_t j = 0; j < image->height; j++)
            new_image.data[ i * image->height + j] = image->data[(image->height - j - 1) * image->width + i];

    return new_image;
}
