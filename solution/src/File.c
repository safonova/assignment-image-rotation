#include "../include/File.h"
#include "../include/Utils.h"
#include "../include/bmp_processing.h"
#include <inttypes.h>
#include <stdio.h>

static const char *const cf_error_messages_read[] = {
        [READ_OK] = "File is read.",
        [READ_ERROR_DATA] = "File is not read. Error with array of pixels. ",
        [READ_ERROR_BITS] = "File is not read. Error with bits.",
        [READ_ERROR_HEADER] = "File is not read. Error with header. ",
        [READ_ERROR_SIGNATURE] = "File is not read. Error with signature. ",
        [READ_ERROR_CONDITIONS] = "File is not read. Error with conditions."
};
static const char *const cf_error_messages_write[] = {
        [WRITE_OK] = "File is written",
        [WRITE_ERROR_DATA] = "File is not written. Error with array of pixels.",
        [WRITE_ERROR_HEADER] = "File is not written. Error with header. "
};

static const char *const cf_error_messages_close[] = {
        [CLOSE_ERROR] = "File is not closed.",
        [CLOSE_OK] = "File is closed."
};
static const char *const rights_str[] = {
        [WRITE_ONLY] = "wb",
        [READ_ONLY] = "rb"
};

struct file_check open_file( const char *path, enum rights rights1){
    struct file_check fileCheck;
    fileCheck.file = fopen(path,rights_str[rights1]);
    if(fileCheck.file == NULL) fileCheck.result = NO;
    else fileCheck.result = YES;
    return fileCheck;
}


enum close_file_status close_file(FILE *file) {
    if (fclose(file) != EOF) {
        error_print(cf_error_messages_close[CLOSE_OK]);
        return CLOSE_OK;
    } else {
        error_print(cf_error_messages_close[CLOSE_ERROR]);
        return CLOSE_ERROR;
    }
}

enum read_status read_file(FILE *file, struct image *image) {

    struct Bmp_header bmpHeader;

    if (fread(&bmpHeader, sizeof(bmpHeader), 1, file) != 1) {
        error_print(cf_error_messages_read[READ_ERROR_HEADER]);
        return READ_ERROR_HEADER;
    }
    if (check_bmp_header(bmpHeader) == false) {
        return READ_ERROR_CONDITIONS;
    }

    *image = create_image_by_bmp(&bmpHeader);

    for (uint64_t i = 0; i < image->height; i++) {
        if (fread(&(image->data[i * image->width]), sizeof(struct pixel), image->width, file) != image->width) {
            error_print(cf_error_messages_read[READ_ERROR_DATA]);
            return READ_ERROR_DATA;
        }
        if (fseek(file, padding(image->width), SEEK_CUR) != 0) {
            error_print(cf_error_messages_read[READ_ERROR_BITS]);
            return READ_ERROR_BITS;
        }
    }

    error_print(cf_error_messages_read[READ_OK]);
    return READ_OK;
}


enum write_status write_file(FILE *file, struct image const *image) {
    struct Bmp_header bmpHeader = made_bmp_header(image);
    if (fwrite(&bmpHeader, sizeof(bmpHeader), 1, file) != 1) {
        error_print(cf_error_messages_write[WRITE_ERROR_HEADER]);
        return WRITE_ERROR_HEADER;
    }
    const uint8_t padding1 = padding(image->width);

    for (uint64_t i = 0; i < image->height; i++) {
        if (fwrite(&(image->data[i * image->width]), sizeof(struct pixel), image->width, file) != image->width) {
            error_print(cf_error_messages_write[WRITE_ERROR_DATA]);
            return WRITE_ERROR_DATA;
        }
        if (fseek(file, padding1, SEEK_CUR) != 0) {
            error_print(cf_error_messages_write[WRITE_ERROR_DATA]);
            return WRITE_ERROR_DATA;
        }
    }
    error_print(cf_error_messages_write[WRITE_OK]);
    return WRITE_OK;
}
