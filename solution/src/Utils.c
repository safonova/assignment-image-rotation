#include "../include/Utils.h"
#include <malloc.h>
#include <stdio.h>


void error_print(const char* status){
    fprintf(stderr,"\n%s",status);
}

void free_image(struct image img){
    free(img.data);
}
